#include<stdio.h>
#include<math.h>
int main()
{
 int a,b,c;
 float determinant,result,root1,root2,realpart,imaginarypart;
   printf("Enter the coefficient a,b,c");
   scanf("%d%d%d",&a,&b,&c);
   determinant=(b*b)-(4*a*c);
if(determinant>0)
 {
   root1=(-b-sqrt(determinant))/(2*a);
   root2=(-b+sqrt(determinant))/(2*a);
   printf("root1=%.2f and root2=%.2f",root1,root2);
 }
else if(determinant==0)
 {
   root1=-b/(2*a);
   root2=-b/(2*a);
   printf("root1=%.2f and root2=%.2f",root1,root2);
 }
else
 {
   realpart=-b/(2*a);
   imaginarypart=sqrt(-determinant)/(2*a);
   printf("root1=%.2f+%.2f i and root2=%.2f + %.2f i",realpart,imaginarypart,realpart,imaginarypart);
 }
 return 0;
}